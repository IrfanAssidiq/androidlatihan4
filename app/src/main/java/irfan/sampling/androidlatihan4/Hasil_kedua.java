package irfan.sampling.androidlatihan4;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

public class Hasil_kedua extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.hasil_kedua);
        TextView tv_ = findViewById(R.id.tv_);

        Bundle b = getIntent().getExtras();
        String a = b.getString("kode");

        tv_.setText(a);
    }
}
