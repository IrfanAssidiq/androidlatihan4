package irfan.sampling.androidlatihan4.tampilanDepan;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import irfan.sampling.androidlatihan4.R;

public class HalamanKelima extends Fragment {

    /**
     * method untuk membuat/inisialisasi fragment
     * @param idx
     * @return
     */
    public static HalamanKelima newInstance(int idx){
        HalamanKelima fragment = new HalamanKelima();
        Bundle b = new Bundle();
        b.putInt("index", idx);
        fragment.setArguments(b);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.halaman_kelima, container, false);
        ListView lv = v.findViewById(R.id.lv);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(getActivity(), "list ke : "+ (i+1), Toast.LENGTH_SHORT ).show();
            }
        });

        return v;
    }
}
