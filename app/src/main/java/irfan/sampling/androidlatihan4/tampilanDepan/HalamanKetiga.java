package irfan.sampling.androidlatihan4.tampilanDepan;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import irfan.sampling.androidlatihan4.R;

public class HalamanKetiga extends Fragment {

    /**
     * method untuk membuat/inisialisasi fragment
     * @param idx
     * @return
     */
    public static HalamanKetiga newInstance(int idx){
        HalamanKetiga fragment = new HalamanKetiga();
        Bundle b = new Bundle();
        b.putInt("index", idx);
        fragment.setArguments(b);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.halaman_ketiga, container, false);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menubaru, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu1:{
                Toast.makeText(getActivity(), "Menu 1", Toast.LENGTH_SHORT).show();
                return true;
            }
            case R.id.menu2:{
                Toast.makeText(getActivity(), "Menu 2", Toast.LENGTH_SHORT).show();
                return  true;
            }
            case R.id.menu3:{
                Toast.makeText(getActivity(), "Menu 3", Toast.LENGTH_SHORT).show();
                return  true;
            }
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
