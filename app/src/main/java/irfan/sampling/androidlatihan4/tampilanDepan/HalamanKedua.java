package irfan.sampling.androidlatihan4.tampilanDepan;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import irfan.sampling.androidlatihan4.Hasil_kedua;
import irfan.sampling.androidlatihan4.R;

public class HalamanKedua extends Fragment {

    /**
     * method untuk membuat/inisialisasi fragment
     * @param idx
     * @return
     */
    public static HalamanKedua newInstance(int idx){
        HalamanKedua fragment = new HalamanKedua();
        Bundle b = new Bundle();
        b.putInt("index", idx);
        fragment.setArguments(b);
        return fragment;
    }

    EditText et;
    Button btn;
    String a;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.halaman_kedua, container, false);
         et = v.findViewById(R.id.et_);
         btn = v.findViewById(R.id.btn_);
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle b = new Bundle();
                a = et.getText().toString();
                if(a.equals("")){
                    a = "default";
                }
                b.putString("kode", a);
                startActivityForResult(new Intent(getActivity(), Hasil_kedua.class).putExtras(b), 10212);

            }
        });
    }
}
