package irfan.sampling.androidlatihan4.tampilanDepan;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import irfan.sampling.androidlatihan4.R;

public class HalamanPertama extends Fragment {

    Button btn;
    /**
     * method untuk membuat/inisialisasi fragment
     * @param idx
     * @return
     */
    public static HalamanPertama newInstance(int idx){
        HalamanPertama fragment = new HalamanPertama();
        Bundle b = new Bundle();
        b.putInt("index", idx);
        fragment.setArguments(b);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.halaman_pertama, container, false);
        btn = v.findViewById(R.id.btn_);

//        btn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Toast.makeText(getActivity(), "Test Toast", Toast.LENGTH_SHORT).show();
//            }
//        });

        return v;
    }
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity(), "Test Toast", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
