package irfan.sampling.androidlatihan4.tampilanDepan;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import irfan.sampling.androidlatihan4.R;

public class HalamanKeempat extends Fragment {

    /**
     * method untuk membuat/inisialisasi fragment
     * @param idx
     * @return
     */
    public static HalamanKeempat newInstance(int idx){
        HalamanKeempat fragment = new HalamanKeempat();
        Bundle b = new Bundle();
        b.putInt("index", idx);
        fragment.setArguments(b);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.halaman_keempat, container, false);
    }
}
